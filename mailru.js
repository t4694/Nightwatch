﻿module.exports = {
  tags: ['mail'],


  'Demo test mail': function (browser) {
  const secondSearchResult='//ul/li [@number="2"]//span/a [contains(@href,"https://wikiway.com")]'
    browser
    .url('https://mail.ru/')
    .pause(2000)
    .assert.visible('body', 'страница https://mail.ru/ загрузилась'); //проверка страницы
      
    browser
    .useXpath()
    .assert.visible('//input [@name="q"]', 'Cтрока поиска отобразилась');
    
    browser
    .useXpath()
    .click('//input [@name="q"]')
    .setValue('//input [@name="q"]', 'Великая китайская стена');
    
    browser
    .useXpath()
    .click('//button [@type="submit"] ')
    .assert.urlContains('https://go.mail.ru/search?q=', 'Найдено по запросу Великая китайская стена');
    
    browser.assert.visible(secondSearchResult, 'в поисковой выдаче 2й результат с ресурса https://wikiway.com/ ');
    
    browser.pause(10000);
    browser.end();
  }
};
