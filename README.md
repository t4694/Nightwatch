**Run tests**

1. Clone the project
2. install Node.js:
- download url:  https://nodejs.org/en/download/

3. Install Nightwatch from NPM:
`npm install nightwatch -g`

4. Start selenium-server-standalone-3.5.3.jar, write in terminal:
`java selenium-server-standalone-2.44.0.jar -port 9515`

5. run tests mailru.js or nightwatch.conf.js, write in terminal:
`npx nightwatch <mailru.js>`
in <>-name file.
