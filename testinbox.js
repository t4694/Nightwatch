module.exports = {
    tags: ['mail'],

    'Demo test mailMessage': function (browser) {
        const log='usertest1995'; //логин
        const pass='srtjerysujh'; //пароль
        const adr='https://mail.ru/';
        const pathSelectorLogin='//input [@name="login"]'; //ввод поле логин
        const clicklogin='//button [@type="button"] [@data-testid="enter-password"]'; //кнопка перехода логина
        const pathSelectorPassword= '//input [@type="password"] '; //ввод поле пароь
        const clickPassword='//button [@data-testid="login-to-mail"] [@type="button"]'; //кнопка перехода пароль
        const selectAll='//span [@class="button2__explanation"]' //кнопка выделить всё
        const points='//* [@class="ico ico_16-more-hor ico_size_s"]' //кпока "троеточие"
        const readMessage='//div [@class="llc__item llc__item_correspondent"]' //признак прочитанного письмо
        const markNotRead='//span [@class="list-item__ico list-item__ico_unread"]' //отметить непрочитанными
        const countMessage='//span [@class="ll-crpt"]' //письмо
        let rezValueCount;
        let i;

        browser.maximizeWindow ();
        browser
            .url(adr)
            .pause(2000)
            .assert.visible('body', '*****Страница https://mail.ru/ загрузилась*****');

        browser
            .useXpath()
            .assert.visible(pathSelectorLogin, '*****Cтрока логина отобразилась*****')
            .sendKeys(pathSelectorLogin, log)
            .click(clicklogin);

        browser
            .useXpath()
            .assert.visible(pathSelectorPassword, '*****Cтрока пароля отобразилась*****')
            .sendKeys(pathSelectorPassword, pass)
            .click(clickPassword);

        browser
            .useXpath()
            .waitForElementVisible(countMessage,15000)
            .findElements(countMessage, function(result) { //подсчитываем количество писем
                for (i = 0; i < result.value.length; i++) {
                    ;
                }
                if (i >= 3) {
                    rezValueCount = i;
                    console.log('*****Количество писем *****', i);
                    //если есть не прочитанные:
                    browser.elements('xpath',readMessage,function (result) {
                            if (result.value.length) {
                            //нажимаем на кнопку выделить всё
                            browser.useXpath().waitForElementVisible(selectAll, 10000).click(selectAll);
                            //нажимаем на "..."
                            browser.useXpath().waitForElementVisible(points, 20000).click(points);
                            //в выподающем меню нажимаем отметить непрочитанным
                            browser.useXpath().waitForElementVisible(markNotRead, 10000).click(markNotRead);
                            //снять выделенное
                            browser.useXpath().waitForElementVisible(selectAll, 10000).click( selectAll);
                            } else {
                                console.log('*****Не прочитанных писем нет*****');

                                   }
                    });
                } else {
                    console.log('*****Мало писем, никто не пишет*****');
                }
            });

        browser.pause(2000);
    },
        'Test read message': function (browser) {
            const messages1='//a [@class="llc js-tooltip-direction_letter-bottom js-letter-list-item llc_normal"]' // письма на почте
            const messages2='//a [@class="llc js-tooltip-direction_letter-bottom js-letter-list-item llc_normal llc_last"]' // последнее письмо на почте
            const readMessage='//div [@class="llc__item llc__item_correspondent"]' // признак прочитанного письма
            let i;
            //открытие писем в цикле
            browser.useXpath().findElements(messages1,function (result) {
                for (i = 0; i < result.value.length; i++) {
                    browser.execute(function (val) {
                        window.open(val);
                    }, [result.value[i]]);
                 //   console.log('ВСЕМ писец', result.value[i]);
                }
            });
            //открытие последнего письма
            browser.useXpath().findElement(messages2,function (result) {
                    browser.execute(function (val) {
                        window.open(val);
                    }, [result.value]);
                    console.log('ВСЕМ писец', result.value);
            });
            browser.pause(20000);
            //проверка количества прочитанных писем
            browser
                .useXpath()
                .waitForElementVisible(readMessage,50000)
                .elements('xpath',readMessage,function (result) {
                for (i = 0; i < result.value.length; i++) {
                    ;
                }
                if(i>=3)
                    console.log('****Все письма прочитаны, количество прочитанных писем****', i)
                    else
                    console.log('****Не все письма были открыты, количество открытых писем****', i)
            });
        //выход с почтового ящика:
        //вызываем всплывающее меню
        browser.useXpath().click('//*[@id="ph-whiteline"]/div/div[2]/div[2]/span[2]');
        //нажимаем выход
        browser.useXpath().click('//a [@rel="nofollow"]');
        browser.pause(2000);
        browser.end();
    }
};
